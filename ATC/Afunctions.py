import numpy as np
from scipy import signal
import scipy


#import matplotlib.pyplot as plt
#import pickle as pkl
#import scdmsPyTools.Traces.Noise as btN
#import pandas import DataFrame,MultiIndex,Series
#import uproot
#import qetpy as qp
#import uproot
#import scipy.optimize as sco


def moving_average(x, n=5):
    return np.convolve(x, np.ones(n), 'valid') / n
    
def filterSignal(sig,freqHigh=40e3,freqLow=None,filtOrder=3,fs=625e3,truncate=False):
    #Truncate removes ends based on low-pass frequency (freqHigh)
    from scipy import signal
    if freqHigh > fs//2:
        print('Cutoff frequency too high, set to max value of',fs//2-1)
        freqHigh = fs//2-1
    if freqLow != None:
        b, a = signal.butter(filtOrder,Wn=(freqLow,freqHigh),btype='bandpass',fs=fs)
    else:
        b, a = signal.butter(filtOrder,Wn=freqHigh,btype='low',fs=fs)
    out = signal.filtfilt(b, a, sig)
    if truncate:
        n=sig.shape[-1]
        xaxis = np.arange(n)/fs
        cutoff = np.arange(n)[xaxis>(1/freqHigh)][0]
        out = out[...,cutoff:-cutoff]
        
    return out


def uniqueCount(array):
    uniques = {}
    for val in np.unique(array):
        uniques[val] = np.sum(array==val)
    return uniques


#Placeholder, this is how to do baseline subtraction!
def baseline_subtract(traces,baselines):
    return traces - baselines.reshape(baselines.shape+(1,))


def alignPulses(pulse,alignTo='self',p=0.2,thresh=50,maxShift=100,returnOnlyRoll=False,alignType='pMax',printRoll=False):
    #alignType can be  pMax, max, threshold, input
    #if input is selected, a value/array must be input under alignTo which matches the pulse dimensions.
            
    if alignType not in ['pMax', 'max', 'threshold','input']:
        print('Unrecognized alignType. Please choose from pMax, max, threshold, input.' )
        return pulse
    
    if alignType == 'input':
        if pulse.ndim==1:
            if type(alignTo)==int:
                rollBy = alignTo
            if len(alignTo)==1:
                rollBy = alignTo[0]
            else:
                print('For single pulse, alignTo must be single integer value.')
                return pulse
            
            return np.roll(pulse,rollBy)
        else:
            if len(alignTo)!=len(pulse):
                print('alignTo must be same length has pulse array.')
                return pulse
            
            return np.array([np.roll(pulseI,rollBy) for pulseI,rollBy in zip(pulse,alignTo)])
                
    if alignTo=='def':
        if pulse.shape[-1]>10000:
            alignTo=16200
        else:
            alignTo=500
    if alignTo=='self':
        if pulse.ndim==1:
            print('Align to self requires >1 dimension')
            return pulse
        align1 = alignPulses(pulse,alignTo=0,p=p,thresh=thresh,maxShift=pulse.shape[-1],returnOnlyRoll=True,alignType=alignType)
        alignTo = -int(np.median(align1))
        
    if pulse.ndim>1:
        return np.array([alignPulses(pulseI,alignTo,p,thresh,maxShift,returnOnlyRoll,alignType,printRoll=False) for pulseI in pulse])
    
    if alignType=='pMax':
        minIndex = np.max([0,alignTo-maxShift])
        maxIndex = np.min([len(pulse),alignTo+maxShift])
        maxV = np.max(pulse[minIndex:maxIndex])
        aboveTh = np.arange(minIndex,maxIndex)[pulse[minIndex:maxIndex]>maxV*p]
        if len(aboveTh)>0:
            rollBy = alignTo - aboveTh[0]
        else:
            rollBy = 0
    
    elif alignType=='max':
        minIndex = np.max([0,alignTo-maxShift])
        maxIndex = np.min([len(pulse),alignTo+maxShift])
        rollBy = alignTo - (np.argmax(pulse[minIndex:maxIndex])+minIndex)
    
    elif alignType=='threshold':
        minIndex = np.max([0,alignTo-maxShift])
        maxIndex = np.min([len(pulse),alignTo+maxShift])
        aboveTh = np.arange(minIndex,maxIndex)[pulse[minIndex:maxIndex]>thresh]
        if len(aboveTh)>0:
            rollBy = alignTo - aboveTh[0]
        else:
            rollBy = 0
    
    if printRoll:
        print('Rolling By:',rollBy)
    if returnOnlyRoll:
        return rollBy
    else:
        return np.roll(pulse,rollBy)


def fixTrigTime(trigTime,fs=625e3):
    ttime=trigTime
    ttimeResets = np.arange(len(ttime)-1)[ttime[1:] < ttime[:-1]] +1
    for ii in range(len(ttimeResets)):
        if (ii == len(ttimeResets)-1):
            ttime[ttimeResets[ii]:]+=(ii+1)*2**32/fs
        else:
            ttime[ttimeResets[ii]:ttimeResets[ii+1]]+=(ii+1)*2**32/fs
        
    return ttime

def getTimeOn(trigTime,fs=625e3):
    ttime = fixTrigTime(trigTime)
    return ttime[-1]-ttime[0]



def addSum(array,axis=1):
    
    if axis==0:
        array=np.append(array,[np.sum(array,axis=0)],axis=0)
    else:
        transI = np.arange(len(array.shape))
        transI[[0,axis]]=transI[[axis,0]]
        array=np.transpose(array,transI)
        array=np.append(array,[np.sum(array,axis=0)],axis=0)
        
        array=np.transpose(array,transI)
    return array



def fallBin(events,fallP=0.5,maxStart=450,maxEnd=950):
    if len(events.shape)>1:
        return np.array([fallBin(eventI) for eventI in events])
    
    maxP = np.argmax(events[maxStart:maxEnd])+maxStart
    maxV = np.max(events[maxStart:maxEnd])
    
    crBool = (events[maxP:]<maxV*fallP)
    if np.sum(crBool)==0:
        return -1
    
    crP = np.arange(len(events[maxP:]))[crBool][0] + maxP
    
    return crP
        

#Some fitting stuff
import scipy.optimize as sco

def normDist(xaxis,mu,sig,n):
        from scipy.stats import norm
        return n*norm.pdf(xaxis,mu,sig)
    
def getNormFit(ofAmps, prange=None,bins=100,printResult=False,boundAmps=False,twoStage=False,printDebug=False,suppressWarning=False):

    ofaSort = np.sort(ofAmps)
    nEvents=len(ofaSort)
    if prange is None:
        r10 = ofaSort[int(nEvents*0.1)]
        r90 = ofaSort[int(nEvents*0.9)]
        rDiff = r90-r10
        prange=(r10-rDiff*0.3,r90+rDiff*0.3)  #Set default plot range to contain 90% of data, plus 30% on each end. This is in case there are a few outliers.
        if printDebug:
            print('Range set to:',prange)
        
    binWidth=(prange[1]-prange[0])/bins
    hist = np.histogram(ofAmps,bins=bins,range=prange)
    xaxis = (hist[1][1:]+hist[1][:-1])/2
    hist = hist[0]        
        
    if boundAmps:
        bounds = [[0]*3,[np.inf]*3]
    else:
        bounds = [[-np.inf,0,0],[np.inf]*3]
    
    
    p0Mean = np.median(ofAmps[(ofAmps>prange[0]) & (ofAmps<prange[1])])
    p0SD = np.std(ofAmps[(ofAmps>prange[0]) & (ofAmps<prange[1])])
    p0N = np.sum([(ofAmps>prange[0]) & (ofAmps<prange[1])])
    if printDebug:
        print('Guesses',p0Mean,p0SD,p0N)
    
    fit = sco.curve_fit(normDist,xaxis,hist,p0=[p0Mean,p0SD,p0N],maxfev=8000,bounds=bounds)[0]
    if twoStage:
        
        
        cutF = np.abs(ofAmps-fit[0])<max([fit[1]*3,binWidth*10])  # Always use at least 10 bins.
        hist = np.histogram(ofAmps[cutF],bins=bins,range=prange)
        xaxis = (hist[1][1:]+hist[1][:-1])/2
        hist = hist[0]
        
        p0Mean = np.median(ofAmps[cutF])
        p0SD = np.std(ofAmps[cutF])
        p0N = np.sum([(ofAmps>prange[0]) & (ofAmps<prange[1])])
        if printDebug:
            print('Guesses-2',p0Mean,p0SD,p0N)
        

        fit = sco.curve_fit(normDist,xaxis,hist,p0=[p0Mean,p0SD,p0N],maxfev=8000,bounds=bounds)[0]
        
    if (not suppressWarning) & (fit[1]<binWidth*5):
        print(f'Warning: SD ({fit[1]}) less than 5 bins ({binWidth*5}), try using more bins.')
    if printResult:
        print(f'{np.round(fit[0],3)} ({np.round(fit[1],3)})')
    return xaxis,fit





def normDist2(xaxis,mu1,sig1,n1,mu2,sig2,n2):
        from scipy.stats import norm
        return n1*norm.pdf(xaxis,mu1,sig1) + n2*norm.pdf(xaxis,mu2,sig2)
    
    
from scipy.signal import find_peaks    
def getNormFit2(ofAmps, prange=None,bins=100,printResult=False,printDebug=False):
    
    ofaSort = np.sort(ofAmps)
    nEvents=len(ofaSort)
    
    if prange is None:
        r10 = ofaSort[int(nEvents*0.1)]
        r90 = ofaSort[int(nEvents*0.9)]
        rDiff = r90-r10
        prange=(r10-rDiff*0.3,r90+rDiff*0.3)  #Set default plot range to contain 90% of data, plus 30% on each end. This is in case there are a few outliers.
        if printDebug:
            print('Range set to:',prange)
            
    hist = np.histogram(ofAmps,bins=bins,range=prange)
    xaxis = (hist[1][1:]+hist[1][:-1])/2
    hist = hist[0]

    binWidth = (prange[1]-prange[0])/bins
    peaks,heights = find_peaks(hist,prominence=np.sqrt(np.max(hist)),width=binWidth/100)  #Set width small just to include the outputs.
    if printDebug:
        print('Debug Peaks,Heights:',peaks,heights)
    heights,widths = heights['prominences'], heights['widths']
    top2 = peaks[np.argsort(heights)[-2:]]
    guess1,guess2 = xaxis[top2]
    guessSD1,guessSD2 = widths[np.argsort(heights)[-2:]]*0.65*binWidth
    guessN1,guessN2 = heights[np.argsort(heights)[-2:]]*3

    if guess1>guess2:
        guess1,guess2 = guess2,guess1
        guessSD1,guessSD2 = guessSD2,guessSD1
        guessN1,guessN2 = guessN2,guessN1

    if printDebug:
        print('Debug guesses:',guess1,guessSD1,guessN1,guess2,guessSD2,guessN2)

    fit = sco.curve_fit(normDist2,xaxis,hist,p0=[guess1,guessSD1,guessN1,guess2,guessSD2,guessN2],maxfev=8000)[0]
    if False:
        cutF = np.abs(ofAmps-fit[0])<fit[1]*3
        hist = np.histogram(ofAmps[cutF],bins=bins,range=prange)
        xaxis = (hist[1][1:]+hist[1][:-1])/2
        hist = hist[0]
        guess = fit[0]
        fit = sco.curve_fit(normDist2,xaxis,hist,p0=[guess,20,1000,guess,20,1000],maxfev=8000)[0]
    if printResult:
        print(f'{np.round(fit[0],3)} ({np.round(fit[1],3)})  -  {np.round(fit[3],3)} ({np.round(fit[4],3)})')
    return xaxis,fit


#This version individually fits all of the peaks of the given criteria.
def getNormFitN(ofAmps, prange=None,bins=None,binWidth=None,printResult=False,printDebug=False,minHeight=100,minWidth=1):
           
    
    
    ofaSort = np.sort(ofAmps)
    nEvents=len(ofaSort)
    
    if prange is None:
        r10 = ofaSort[int(nEvents*0.1)]
        r90 = ofaSort[int(nEvents*0.9)]
        rDiff = r90-r10
        prange=(r10-rDiff*0.3,r90+rDiff*0.3)  #Set default plot range to contain 90% of data, plus 30% on each end. This is in case there are a few outliers.
        if printDebug:
            print('Range set to:',prange)
    
    if bins is not None and binWidth is not None:
        print("Do not use both bins and binWidth")
        return 0
    
    if bins is None:
        if binWidth is None:
            bins=500
            binWidth = (prange[1]-prange[0])/bins
        else:
            bins = int((prange[1]-prange[0])/binWidth)
    else:
        binWidth = (prange[1]-prange[0])/bins
    if printDebug:
        print('Bins:',bins,' Bin width:',binWidth)
            
            
    hist = np.histogram(ofAmps,bins=bins,range=prange)
    xaxis = (hist[1][1:]+hist[1][:-1])/2
    hist = hist[0]

    
    pos,heights = find_peaks(hist,height=minHeight,prominence=minHeight,width=minWidth/binWidth)  #Set width small just to include the outputs.
    
    heights,widths = heights['peak_heights'], heights['widths']
    
    sort = np.argsort(pos)
    pos=xaxis[pos[sort]]
    heights= heights[sort]
    widths=widths[sort]*binWidth
    
    if printDebug:
        for ii in range(len(pos)):
            print(f'Peak find {ii+1}:  {np.round(pos[ii])}({np.round(widths[ii])})  -  {np.round(heights[ii])}')

    fits = []
    for ii in range(len(pos)):
        fits.append(getNormFit(ofAmps,prange=(pos[ii]-widths[ii]*5,pos[ii]+widths[ii]*5), bins=int(widths[ii]*10/binWidth),printResult=printResult)[1])
    return xaxis,np.array(fits)
                    


    
    top2 = pos[np.argsort(heights)[-2:]]
    guess1,guess2 = xaxis[top2]
    guessSD1,guessSD2 = widths[np.argsort(heights)[-2:]]*0.65*binWidth
    guessN1,guessN2 = heights[np.argsort(heights)[-2:]]*3

    if guess1>guess2:
        guess1,guess2 = guess2,guess1
        guessSD1,guessSD2 = guessSD2,guessSD1
        guessN1,guessN2 = guessN2,guessN1

    if printDebug:
        print('Debug guesses:',guess1,guessSD1,guessN1,guess2,guessSD2,guessN2)

    fit = sco.curve_fit(normDist2,xaxis,hist,p0=[guess1,guessSD1,guessN1,guess2,guessSD2,guessN2],maxfev=8000)[0]
    if False:
        cutF = np.abs(ofAmps-fit[0])<fit[1]*3
        hist = np.histogram(ofAmps[cutF],bins=bins,range=prange)
        xaxis = (hist[1][1:]+hist[1][:-1])/2
        hist = hist[0]
        guess = fit[0]
        fit = sco.curve_fit(normDist2,xaxis,hist,p0=[guess,20,1000,guess,20,1000],maxfev=8000)[0]
    if printResult:
        print(f'{np.round(fit[0],3)} ({np.round(fit[1],3)})  -  {np.round(fit[3],3)} ({np.round(fit[4],3)})')
    return xaxis,fit


def normDist2B(xaxis,mu1,sig1,n1,mu2,sig2,n2,base=0):  #This version with offset
        from scipy.stats import norm
        return n1*norm.pdf(xaxis,mu1,sig1) + n2*norm.pdf(xaxis,mu2,sig2) + base

def getNormFit2B(ofAmps, prange=None,bins=100,printResult=False,printDebug=False):#This version with offset
    
    ofaSort = np.sort(ofAmps)
    nEvents=len(ofaSort)
    
    if prange is None:
        r10 = ofaSort[int(nEvents*0.1)]
        r90 = ofaSort[int(nEvents*0.9)]
        rDiff = r90-r10
        prange=(r10-rDiff*0.3,r90+rDiff*0.3)  #Set default plot range to contain 90% of data, plus 30% on each end. This is in case there are a few outliers.
        if printDebug:
            print('Range set to:',prange)
            
    hist = np.histogram(ofAmps,bins=bins,range=prange)
    xaxis = (hist[1][1:]+hist[1][:-1])/2
    hist = hist[0]

    binWidth = (prange[1]-prange[0])/bins
    peaks,heights = find_peaks(hist,prominence=np.sqrt(np.max(hist)),width=binWidth/100)  #Set width small just to include the outputs.
    if printDebug:
        print('Debug Peaks,Heights:',peaks,heights)
    heights,widths = heights['prominences'], heights['widths']
    top2 = peaks[np.argsort(heights)[-2:]]
    guess1,guess2 = xaxis[top2]
    guessSD1,guessSD2 = widths[np.argsort(heights)[-2:]]*0.65*binWidth
    guessN1,guessN2 = heights[np.argsort(heights)[-2:]]*3
    guessBase = np.sort(hist)[int(bins*0.1)]

    if guess1>guess2:
        guess1,guess2 = guess2,guess1
        guessSD1,guessSD2 = guessSD2,guessSD1
        guessN1,guessN2 = guessN2,guessN1

    if printDebug:
        print('Debug guesses:',guess1,guessSD1,guessN1,guess2,guessSD2,guessN2,guessBase)

    fit = sco.curve_fit(normDist2B,xaxis,hist,p0=[guess1,guessSD1,guessN1,guess2,guessSD2,guessN2,guessBase],maxfev=8000)[0]
    if False:
        cutF = np.abs(ofAmps-fit[0])<fit[1]*3
        hist = np.histogram(ofAmps[cutF],bins=bins,range=prange)
        xaxis = (hist[1][1:]+hist[1][:-1])/2
        hist = hist[0]
        guess = fit[0]
        fit = sco.curve_fit(normDist2B,xaxis,hist,p0=[guess,20,1000,guess,20,1000],maxfev=8000)[0]
    if printResult:
        print(f'{np.round(fit[0],3)} ({np.round(fit[1],3)})  -  {np.round(fit[3],3)} ({np.round(fit[4],3)}) - {np.round(fit[6],3)}')
    return xaxis,fit


def line(xaxis,m,b):
    return m*np.array(xaxis)+b
def getLineFit(xdata,ydata,returnError=False):
    if returnError:
        return sco.curve_fit(line,xdata,ydata)
    else:
        return sco.curve_fit(line,xdata,ydata)[0]   

def exponA(xaxis,a,b,c):  #Simple exponential, not related to pulses.
    return a*np.exp(xaxis*b)+c

def getExponFit(xdata,ydata,returnError=False,p0=None):
    if returnError:
        return sco.curve_fit(exponA,xdata,ydata,p0=p0)
    else:
        return sco.curve_fit(exponA,xdata,ydata,p0=p0)[0]
    
    
def subtractBaselines(events,nbins=500):
    baselines = np.median(events[...,:nbins],axis=-1)
    return events - baselines.reshape(baselines.shape+(1,))
    
    
def baselines(traces,range=None,xaxis=None,returnSD=False,returnSlope=False):
    ### No longer used in data processing, see that file for more efficient ways of calculating these.
    
    ### Used in data processing ###
    
    # range can be a tuple giving start and end bin, or a single value. For single value: if >0, uses first N-range bins, if <0 uses last N-range bins.
    # xaxis only used for slope calculation, if desired.
    
    if traces.ndim>1:
        return np.array([baselines(trace,range) for trace in traces])
    
    if range is None:
        range = (None,None)
    elif type(range) is int:
        if range>0:
            range=(None,range)
        elif range<0:
            range=(range,None)
        else:
            raise ValueError('range should not be 0')
    elif len(range)==2:
        pass
    else:
        raise ValueError('range not in expected form, either integer or tuple')
    
    baseline = np.median(traces[range[0]:range[1]])
    
    if (returnSD is None)&(returnSlope is None):
        return baseline
    else:
        rlist=[baseline]
    
    if returnSD:
        rlist.append(np.std(traces[range[0]:range[1]]))
        
    if returnSlope:
        if xaxis is None:
            xaxis=np.arange(traces.shape[-1])
        baselineSlope = scipy.stats.linregress(xaxis[range[0]:range[1]],traces[range[0]:range[1]]).slope
    
        
    elif len(range)==2:
        return np.median(traces[range[0]:range[1]])
    
    else:
        
        return 0
    
    
def baselineSlope(traces,range=None,xaxis=None,commonVars=None):
    # xaxis can be input if desired
    
    if xaxis is None:
        xaxis=np.arange(traces.shape[-1])
        
    if range is None:
        range = (None,None)
    elif type(range) is int:
        if range>0:
            range=(None,range)
        elif range<0:
            range=(range,None)
        else:
            raise ValueError('range should not be 0')
    elif len(range)==2:
        pass
    else:
        raise ValueError('range not in expected form, either integer, pair of values, or None')
        
    xI = xaxis[range[0]:range[1]]
    #Use common vars to speed up calculation on many traces.
    if commonVars is None:
        xmean = xI.mean()
        den = np.sum((xI-xmean)**2)
        commonVars={'xmean':xmean,'den':den}
    else:
        xmean=commonVars['xmean']
        den=commonVars['den']
    
    if traces.ndim>1:
        return np.array([baselineSlope(trace,range=range,xaxis=xaxis,commonVars=commonVars) for trace in traces])
        
    #Slope for each trace
    tI=traces[range[0]:range[1]]
    return np.sum((xI-xmean)*(tI-tI.mean()))/den
    
    
# Returns the full Amp/Chi2 trace
def ofAmp(traces,template,noise,normalizeTemplate=False,returnChi2=False,commonVars=None):
    
    if commonVars is not None:
        Tf = commonVars['Tf']
        J = commonVars['J']
        Zterm = commonVars['Zterm']
    else:
        if normalizeTemplate:
            template = template/np.max(template)
        Tf = np.fft.rfft(template)
        print(len)
        J = noise**2
        J[0]=np.inf
        Zterm=np.sum(Tf.conjugate()*Tf/J)
        commonVars={}
        commonVars['Tf']=Tf
        commonVars['J']=J
        commonVars['Zterm']=Zterm

    if traces.ndim>1:
        return np.array([ofAmp(trace,template,noise,normalizeTemplate,returnChi2,commonVars=commonVars) for trace in traces])
    
    Sf = np.fft.rfft(traces)
    Xterm = np.sum(Sf.conjugate()*Sf/J)
    Yifft = np.real(np.fft.irfft(((Tf.conjugate()*Sf)/J))*Tf.shape[-1])  # Real not done inside irfft as it returns the incorrect result.
    amp = np.real(Yifft/Zterm)
    chi2 = np.real(Xterm-Yifft**2/Zterm)/(Tf.shape[-1]-1) # chi2 per d.o.f.
    
    if returnChi2:
        return amp, chi2
    else:
        return amp


    
    
#Some fitting functions related to exponential fitting. 
fs=625e3
fs1=fs/1e6


    
#Exponentials
if False:  #First version, left in case I need to reference.
    def expon(t, tau_rise=20e-6, tau_fall=300e-6):
        return (1.0-np.exp(-t/tau_rise))*np.exp(-t/tau_fall)

    def exponN(t, tau_rise=20e-6, tau_fall=300e-6):
        exp = expon(t, tau_rise=tau_rise, tau_fall=tau_fall)
        return exp/np.max(exp)

    def expon2(t, tau_rise=20e-6, tau_fall1=300e-6, tau_fall2=1000e-6,p1=0.5):
        return (1.0-np.exp(-t/tau_rise))*((p1)*np.exp(-t/tau_fall1)+(1-p1)*np.exp(-t/tau_fall2))

    def expon2N(t, tau_rise=20e-6, tau_fall1=300e-6, tau_fall2=1000e-6,p1=0.5):
        exp = expon2(t, tau_rise=tau_rise, tau_fall1=tau_fall1, tau_fall2=tau_fall2, p1=p1)
        return exp/np.max(exp)


    def expon2NL2M(t, tau_fall2=2000,p1=0.5, amp=1, tshift = 0):  #tau fall1 locke
        exp = expon2(t, tau_rise=25, tau_fall1=100, tau_fall2=tau_fall2, p1=p1)
        if tshift >= 1:
            tshift = int(tshift)
            exp[tshift:] = exp[:-tshift]
            exp[:tshift] = 0
        return exp/np.max(exp)*amp

    def expon2NL2MFFT(t, tau_fall2=2000,p1=0.5, amp=1):
        return np.abs(np.fft.rfft(expon2NL2M(t=t,tau_fall2=tau_fall2,p1=p1,amp=amp)))

    def fitAmp(fitA,traceLength=2000,fs=625e3):  #Allow for traceLength change, but always use the same value regardless of fit trace Length so normalization matches.
        fitANorm = np.sum(expon2NL2M(np.arange(traceLength)/fs*1e6,1,1,12000))  #100% fall 1, max chosen s.t. fitsA vs PES1OFamps is 1:1 up to 1uA
        return np.sum(expon2NL2M(np.arange(traceLength)/fs*1e6,fitA[0],fitA[1],fitA[2]))/fitANorm


    
if False:
    #Second version, left in case I need to reference, similar to first verison.
    def expon1(t,tau_rise=20,tau_fall1=50,amp1=1,t_offset=0,fs1=fs1):
        if t_offset>t[-1]:
            t_offset=t[-1]
        elif t_offset<0:
            t_offset=0
        prePulse = int(t_offset*fs1)+1
        t=t[prePulse:]
        exp = (1.0-np.exp(-(t-t_offset)/tau_rise))*(amp1*np.exp(-(t-t_offset)/tau_fall1))
        exp=np.insert(exp,0,np.zeros(prePulse))
        return exp

    def expon1F(t,tau_rise=20,tau_fall1=50,tau_fall2=200,amp1=1,amp2=0,t_offset=0,fs1=fs1):
        return np.abs(np.fft.rfft(expon1(t,tau_rise,tau_fall1,amp1,t_offset,fs1)))

    def expon2(t,tau_rise=20,tau_fall1=50,tau_fall2=200,amp1=1,amp2=0,t_offset=0,fs1=fs1):
        if t_offset>t[-1]:
            t_offset=t[-1]
        elif t_offset<0:
            t_offset=0
        prePulse = int(t_offset*fs1)+1
        t=t[prePulse:]
        exp = (1.0-np.exp(-(t-t_offset)/tau_rise))*(amp1*np.exp(-(t-t_offset)/tau_fall1)+amp2*np.exp(-(t-t_offset)/tau_fall2))
        exp=np.insert(exp,0,np.zeros(prePulse))
        return exp

    def expon2F(t,tau_rise=20,tau_fall1=50,tau_fall2=200,amp1=1,amp2=0,t_offset=0,fs1=fs1):
        return np.abs(np.fft.rfft(expon2(t,tau_rise,tau_fall1,tau_fall2,amp1,amp2,t_offset,fs1)))

    def expon3(t,tau_rise=20,tau_fall1=50,tau_fall2=200,tau_fall3=2000,amp1=1,amp2=0,amp3=0,t_offset=0,fs1=fs1):
        if t_offset>t[-1]:
            t_offset=t[-1]
        elif t_offset<0:
            t_offset=0
        prePulse = int(t_offset*fs1)+1
        t=t[prePulse:]
        exp = (1.0-np.exp(-(t-t_offset)/tau_rise))*(amp1*np.exp(-(t-t_offset)/tau_fall1)+amp2*np.exp(-(t-t_offset)/tau_fall2)+amp3*np.exp(-(t-t_offset)/tau_fall3))
        exp=np.insert(exp,0,np.zeros(prePulse))
        return exp

    def expon3F(t,tau_rise=20,tau_fall1=50,tau_fall2=200,tau_fall3=2000,amp1=1,amp2=0,amp3=0,t_offset=0,fs1=fs1):
        return np.abs(np.fft.rfft(expon3(t,tau_rise,tau_fall1,tau_fall2,tau_fall3,amp1,amp2,amp3,t_offset,fs1)))

    
    
# Exeponentials, updated to "Correct" sum-based expontial form.
def expon3(t,tau_rise=20,tau_fall1=50,tau_fall2=200,tau_fall3=2000,amp1=1,amp2=0,amp3=0,t_offset=0):
    if t_offset>t[-1]:
        return np.zeros(t.shape[-1])
    
    if t_offset>0:
        p_offset=np.arange(t.shape[-1])[t>t_offset][0]
        t = t[p_offset:]
        
    exp = amp1*np.exp(-(t-t_offset)/tau_fall1) + amp2*np.exp(-(t-t_offset)/tau_fall2)+amp3*np.exp(-(t-t_offset)/tau_fall3) - (amp1+amp2+amp3)*np.exp(-(t-t_offset)/tau_rise)
    
    if t_offset>0:
        exp=np.insert(exp,0,np.zeros(p_offset))
    
    return exp

def expon2(t,tau_rise=20,tau_fall1=50,tau_fall2=200,amp1=1,amp2=0,t_offset=0):  #Set with amplitude 0 and arbitrary fall time of 1 for extra falls.
    return expon3(t,tau_rise,tau_fall1,tau_fall2,1,amp1,amp2,0,t_offset)
def expon1(t,tau_rise=20,tau_fall1=50,amp1=1,t_offset=0):
    return expon3(t,tau_rise,tau_fall1,1,1,amp1,0,0,t_offset)


import scipy.optimize as sco

from Afunctions import *
def fitExp(event, fs1=fs/1e6, tau_rise=None, fDomFit=False, 
           noiseFFT=None, p0=None, nFalls=2, returnChi2=False,returnFitErrors=False,
           noiseSD=None,returnAmp=False,boundFit=False,bounds=None,
           plotResult=False,fitArgs={},**kwargs):
    import scipy.optimize as sco
    # p0 should contain as necessary: tau_rise, tau_fall 1, tau_fall2, tau_fall3, amp1, amp2, amp3, t_offset
    # exponI is generally just used so the function can pass itself the correct exponential function when iterating over an array.
    # freq-domain fit with NoiseFFT is not working currently.
    # Bounds should be inputted as nParams x 2, including +/-np.inf for unbound/semi-unbound.
    # Default bounds force all values except time positive.
    # fitArgs is a dictionary containing keyword arguments to be passed to the curve_fit function, e.g.  {'xtol' : 1e-3}
    
    if event.ndim>2:
        raise ValueError('Only 1 or 2 dimensions implemented. Needs further work on initial guesses for additional dimensions.')
    
    if nFalls not in [1,2,3]:
        raise ValueError("Only 1-3 Falls programmed.")
    
    #Check if inputted initial guess and bounds has correct number of parameters.
    nParams=nFalls*2
    if tau_rise is None:
        nParams+=1
    if not fDomFit:
        nParams+=1
    
    if (p0 is not None):
        if (len(p0)!=nParams):
            raise ValueError('Incorrect length for p0, expecting',nParams)
    if (bounds is not None):
        if (len(bounds)!=nParams):
            raise ValueError('Incorrect length for bounds, expecting',nParams)

    
    # Set defaults
    #boundsDef = np.array([(0,500),(20,150),(150,1000),(1000,50000),(0,np.inf),(0,np.inf),(0,np.inf),(-np.inf,np.inf)])  #Isolate tau ranges, force positive amplitudes.
    boundsDef = np.array([[0,np.inf]]*7+[[-np.inf,np.inf]])
    
    if p0 is None:
        if True:
            
            if event.ndim==1:
                eventM = event
            else: 
                eventM = np.median(event,axis=0)
            # Guess for tStart based on pulse rise position.
            N = eventM.shape[-1]
            amax = np.argmax(eventM)
            rStart = max([amax-500,0]) #Search for rise point no further than 500 bins left of max.
            r50 = np.arange(N-rStart)[eventM[rStart:]>np.max(eventM)*0.5][0]+rStart
            f50 = np.arange(N-amax)[eventM[amax:]<np.max(eventM)*0.5][0]+amax
            
            #t0guess = np.arange(event.shape[-1])[event>np.max(event)*0.5][0]
            #else:
            #    t0guess=int(np.median( np.array([np.arange(event.shape[-1])[eventI>np.max(eventI)*0.5][0] for eventI in event]) ))

            #t0guessOld = int(np.median(np.argmax(event,axis=-1)))
            #if (t0guess-t0guessOld)>event.shape[-1]*0.1:
            #    t0guess=t0guessOld
            
            riseGuess = (amax-r50)/fs1
            fallGuess = (f50-amax)/fs1
            t0guess = (amax)/fs1-2*riseGuess
        else:
            t0guess = 15950/fs1
        p0Default = np.array([riseGuess,fallGuess,1000,10000,10,1,1,t0guess])
        
    if returnChi2 & (noiseFFT is None) & (noiseSD is None):
        noiseSD=np.median(np.std(event[...,:int(t0guess*fs1)-500]))  # Use median pre-pulse baseline std for chi^2 when nothing is supplied.
            
            
    xaxis = np.arange(0,event.shape[-1])/fs1

    
    #Choose appropriate fitting function and parameter list.
    if 'exponI' in kwargs.keys():
        exponI=kwargs['exponI']
    else: exponI=None
    if 'parList' in kwargs.keys():
        parList=kwargs['parList']
    else: parList=None
    
    if exponI is None:
        if nFalls==1:
            if tau_rise is None:
                parList=[0,1,4,7]
                def exponI(t,*fitParams):
                    return expon1(t,*fitParams)
            else:
                parList=[1,4,7]
                def exponI(t,*fitParams):
                    return expon1(t,tau_rise,*fitParams)

        elif nFalls==2:
            if tau_rise is None:
                parList=[0,1,2,4,5,7]
                def exponI(t,*fitParams):
                    return expon2(t,*fitParams)
            else:
                parList=[1,2,4,5,7]
                def exponI(t,*fitParams):
                    return expon2(t,tau_rise,*fitParams)

        elif nFalls==3:
            if tau_rise is None:
                parList=list(range(8))
                def exponI(t,*fitParams):
                    return expon3(t,*fitParams)
            else:
                parList=list(range(1,8))
                def exponI(t,*fitParams):
                    return expon3(t,tau_rise,*fitParams)
                
    if fDomFit:        
        parList=parList[:-1]
        def exponIF(t,*fitParams):
            return np.abs(np.fft.rfft(exponI(t,*fitParams,p0Default[-1])))
        
    if p0 is None:
        p0 = list(p0Default[parList])
    if boundFit:
        if bounds is None:
            bounds = boundsDef[parList]
    else:
        bounds = (-np.inf, np.inf)  #Default input for curve_fit
    
    #Allow function to self reference for arrays of events
    if event.ndim>1:
        if plotResult:
            print('Should only plot single fits')
            return 0
        return np.array([fitExp(eventI,fs1,tau_rise,fDomFit,noiseFFT,p0,nFalls,returnChi2,noiseSD,returnAmp,boundFit,bounds,exponI=exponI,parList=parList) for eventI in event])
    
    ## Everything below is post self-reference, and so it's run every time.
    
    if np.array(bounds).ndim>1:
        bounds = list(np.array(bounds).T)
    
    if fDomFit:
        eventF = np.abs(np.fft.rfft(event))
    
    chi2=None
    if 'xtol' not in fitArgs.keys():
        fitArgs['xtol']=1e-6
    if 'ftol' not in fitArgs.keys():
        fitArgs['ftol']=1e-3
    if 'maxfev' not in fitArgs.keys():
        fitArgs['maxfev']=int(1e4)
    #try: 
    if True:
        if fDomFit:
            if  (noiseFFT is not None):
                fit,errors = sco.curve_fit(exponIF,xaxis,eventF,p0=p0,bounds=bounds,sigma=noiseFFT,absolute_sigma=False,**fitArgs)
                if returnChi2:
                    fitP = exponIF(xaxis,*fit)
                    chi2 = np.sum(((eventF-fitP)/noiseFFT)**2)/(event.shape[-1]-1)
            else:
                fit,errors = sco.curve_fit(exponIF,xaxis,eventF,p0=p0,**fitArgs)
            fitPulse = exponI(xaxis,*fit,p0Default[-1])

        else:
            fit,errors = sco.curve_fit(exponI,xaxis,event,p0=p0,bounds=bounds,**fitArgs)
            fitPulse = exponI(xaxis,*fit)
            
        if returnChi2 & (chi2 is None):   #This sets the chi^2 either for time-domain fit, or frequency domain fit where NoiseFFT was not supplied. In the latter case, it still gets time-domain chi^2.
                                          # The f-domain case would need a rethink since it won't be aligned due to not fitting t-offset..
            if noiseFFT is not None:
                noiseFFT[0]=0
                SD = np.sqrt(np.sum((noiseFFT**2)))/np.sqrt((noiseFFT.shape[-1])*event.shape[-1]) #Calculate standard deviation based on noiseFFT.
            else:
                SD = noiseSD #Input a value directly.
            chi2 = np.sum(((event-fitPulse)/SD)**2)/(event.shape[-1]-1)
        if returnAmp:
            amp = np.sum(fitPulse)/50  #50 Arbitrarily set as the integral of a standard pulse with height 1.
    #except:
    else:
        print('Fit failed')
        fit = np.zeros(len(p0))
        chi2 = np.inf
        amp = 0

    if plotResult:
        plt.plot(event)
        plt.plot(fitPulse)
        plt.xlim(p0Default[-1]*fs1-500,p0Default[-1]*fs1+2500)
        plt.show()
    
    if not (returnChi2 | returnAmp | returnFitErrors):
        return fit
    else:
        rOut = [fit]
        if returnChi2:
            rOut.append(chi2)
        if returnAmp:
            rOut.append(amp)
        if returnFitErrors:
            rOut.append(errors)

        return tuple(rOut)
    
def getExpIntegral(fit,nFalls=3,tau_rise=None,fs1=fs1,elength=2**15):  
    #Get integral based on fit parameters, setup for 1 fall at thhe moment
    
    nParams=nFalls*2+1
    if tau_rise is None:
        nParams+=1
    if fit.shape[-1]!=nParams:
        print("Fit shape does not match expected number of parameters:",nParams)
        return 0
    
    if fit.ndim>1:
        return np.array([getAmpI(fitI,nFalls,tau_rise,fs1,elength) for fitI in fit])
    
    amps = []
    xaxis=np.arange(0,elength/fs1,1/fs1)
    for ii in range(nFalls):
        if tau_rise is None:
            amps.append(np.sum(expon1(xaxis,*np.array(fit)[[0,ii+1,ii+1+nFalls,-1]],fs1))/50)  #/50 matches fitExp  for arbitrary normalization to typical pulse of height 1.
        else:
            amps.append(np.sum(expon1(xaxis,tau_rise,*np.array(fit)[[ii,ii+nFalls,-1]],fs1))/50)
    return amps







#Ciaran, Tobias and Brandon are adding things to this file here on out, it has some modified functions from ADCRC_Traces and some other things we didn't want to copy paste everywhere
from time import time,sleep
import numpy as np
from IPython.display import clear_output
from scipy.signal import find_peaks


fs=625e3

def convertToDumpNumStr(dumpNumber:int): #TODO - should put in central file that gets imported
    """Converts from dump number to 4 digit string
    
    Example:
    convertToDumpNumStr(74) -> '0074'
    """
    if (dumpNumber < 1) or (dumpNumber > 9999):
        raise ValueError(str(dumpNumber) + " is not a valid dump number, must be between 1 and 9999")
    
    if type(dumpNumber) != int:
        raise ValueError("dumpNumber must be an int! You passed a " + str(type(dumpNumber)))
    
    dumpNumberStr = str(dumpNumber) 
    #Now just need to add the leading zeroes
    
    numZeroesToAdd = 4 - len(dumpNumberStr)
    dumpNumberStr = ("0"*numZeroesToAdd)+dumpNumberStr
    
    return dumpNumberStr



    
def getPeaks(trace,height=40,width=15,prominence=None):
    if trace.ndim>1:
        return np.array([getPeaks(traceI,height,width,prominence) for traceI in trace],dtype=object)
    peaks = find_peaks(trace,height=height,width=width,prominence=prominence)
    xloc = peaks[0]
    yloc = peaks[1]['peak_heights']
    width = peaks[1]['widths']
    count = len(xloc)
    
    return count,xloc,yloc,width

def addPT(events):
    if events.ndim>2:
        return np.array([addPT(event) for event in events])
    
    elif events.ndim==2:
        return np.append(events,[np.sum(events,axis=0)],axis=0)

def convADC(traces,input='ADC',output='Amps',driverGain=1,preampGain=1):
    '''
    Takes np.ndarray as input, returns array as output
    Output/Input can set as Amps (default) or Volts
    driverGain (For Open/Amps) can be provided directly instead of dcrc/chans, either as a single value or a list matching traces second last dimension
    preampGain (For Open) can be provided directly or (to be implemented) interpreted from DCRC.
    '''
    
    
    traces=np.array(traces)


    #Build correctly shaped array for multiplying by traces
    if np.array(traces).ndim==0:  #Accept single values
        conv=1. 
    else:
        traces=np.array(traces)  #Convert lists to array just in case.
        if traces.ndim==1:
            conv=np.array([1.])
        else:
            cshape=[1,]*traces.ndim
            cshape[-2]=traces.shape[-2]
            conv=np.ones(cshape)

    try: 
        len(driverGain) #Is it a listable?
        driverGain=np.array(driverGain) #Turn list into array
        driverGain=driverGain.reshape(conv.shape) #reshape array to match traces
    except: pass #Single values are fine.
    try: 
        len(preampGain)
        preampGain=np.array(preampGain)
        preampGain=preampGain.reshape(conv.shape)
    except: pass


    Rfb = 1185
    LoopGain = 10
    #Rshunt = 0.020 #Not actually used for this calculation.
    LPFGain = 4
    BitsPerVolt = 2**16/8
    AmpsPerVolt = 1/(Rfb*LoopGain*LPFGain*driverGain)
    OpenPerVolt = 1/(0.150*preampGain*driverGain)

    #Convert as Input/Volt * Volt/Output
    if input=='ADC':
        conv*=1/BitsPerVolt
    elif input=='Volts':
        conv*=1
    elif input=='Open':
        conv*=1/OpenPerVolt
    elif input=='Amps':
        conv*=1/AmpsPerVolt
    else:
        raise ValueError('input not set as ADC, Volts, Open or Amps')

    if output=='ADC':
        conv*=BitsPerVolt
    elif output=='Volts':
        conv*=1
    elif output=='Open':
        conv*=OpenPerVolt
    elif output=='Amps':
        conv*=AmpsPerVolt
    else:
        raise ValueError('output not set as ADC, Volts, Open or Amps')


    return traces*conv


def removeOutliers(array,nsig=2,returnMask=False,nPasses=1,useMedianQ=True):
    #Remove outliers only along last axis, each array in last dimension is treated separately.
    array=np.array(array)

    if array.ndim>1:
        raise ValueError('Array ndim>1 not yet implemented')
        #return np.array([self.removeOutliers(arI,nsig,returnMask,nPasses) for arI in array])

    mask=np.full(array.shape[-1],True)
    for passI in range(nPasses):
        if useMedianQ:
            m=np.median(array[mask])
        else:
            m=np.mean(array[mask])
        s=np.std(array[mask])
        mask[mask]=np.abs(array[mask]-m)<nsig*s

    if returnMask:
        return mask
    else:
        return array[mask]

    
# def get_cos_params(wave,freqI,fs=fs): #Is this needed anywhere? I don't think so...
#     if wave.ndim >1:
#         return np.array([get_cos_params(wI,freqI,fs) for wI in wave])
#     n = len(wave)
#     xaxis=np.arange(n)*1e6/fs
#     template = np.exp(1j * 2*np.pi*freqI/1e6*xaxis)
#     corr = 2 / n * template@wave
#     R = np.abs(corr)
#     phi = np.log(corr).imag
#     return R, phi