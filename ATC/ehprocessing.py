import cdms
import numpy as np
import matplotlib.pyplot as plt
import Afunctions as af
import ROOT

#====================== Global variables =================================
chanNames=["PAS1","PBS1","PCS1","PDS1","PES1","PFS1","PAS2","PBS2","PCS2","PDS2","PES2","PFS2"]
series = "23231120_193732"
ProdTag = "Test"
ProdType = "Tests"
noise_filepath = "/sdf/group/supercdms/data/CDMS/CUTE/R36/Processed/"+ProdType+"/"+ProdTag+"/Noise/"+ProdTag+"_Filter_"+series+".root"
raw_filepath = "/sdf/group/supercdms/data/CDMS/CUTE/R36/Raw/"+series
det = 2
nDumps_process = 1 # Number of dumps to process
outputfile = "Pulse_Heights.txt"
#==========================================================================


#====================== Make file names====================================
def make_file_names(series,nDumps):
    """
    Concatenate string to construct raw file names
    :series: series name as str
    :nDumps: number of dumps to process as int
    :return: A list of file names
    """
    file_names = []
    for n in range(nDumps):
        nzeros = "0"*(4-len(str(n)))
        file_names.append(series+"_F"+nzeros+str(n+1)+".mid.gz")
        
    return file_names
#==========================================================================


#================= Low pass filtering =====================================
def low_pass_filter(data, alpha):
    """
    Applies a simple low-pass filter to a data series.

    :param data: a list or array of numeric data
    :param alpha: a smoothing factor between 0 and 1
    :return: a list containing the filtered data
    """
    filtered = [data[0]]  # initialize the filtered data with the first value of the input
    for i in range(1, len(data)):
        filtered.append(alpha * data[i] + (1 - alpha) * filtered[-1])
    return low_padd

#================== Load raw events =========================================
def load_events(filepath,series,nDumps):
    """
    Load traces from the specified dumps
    :filepath: Path to the raw dump location as str
    :series: series name as str
    :nDumps: number of dumps to process as int
    :return: Returns a dictionary of traces from all the dumps. Dictionary keys are channel names.
    """
    
    #Get file names
    filenames = make_file_names(series,nDumps)

    print("Files being loaded: ", filenames)

    #Load data
    traces = {}
    for d in range(nDumps):
        events = cdms.rawio.IO.getRawEvents(filepath, filenames[d], outputFormat = 2, detectorList=[det], channelList = chanNames)
        nEvents = len(events)
        tracelen = len(events[0]["Z"+str(det)]['PFS1'])
        for i in range(nEvents):
            if((events[i]["event"]["TriggerType"]==2) or (events[i]["event"]["TriggerType"]==3)): # Only save randoms
                for c in chanNames:
                    if(i==0):
                        traces[c] = events[i]["Z"+str(det)][c]
                        traces[c] = np.reshape(traces[c],(1,tracelen))
                    else:
                        temp = np.reshape(events[i]["Z"+str(det)][c],(1,tracelen))
                        traces[c]=np.append(traces[c],temp,axis=0)
                                         
    return traces
#===============================================================================

#=============================== Main processing code ==========================

# Open noise file
noisefile = ROOT.TFile.Open(noise_filepath,"READ")

# Load template
#template = []
#for chan in chanNames:
hist = noisefile.Get("zip2/PTTemplateTime")
template = np.array([hist.GetBinContent(i) for i in range(1, hist.GetNbinsX() + 1)])

# Load noise
#noisefft = []
#for chan in chanNames:
hist = noisefile.Get("zip2/PTNoisePSD")
noisefft = np.array([hist.GetBinContent(i) for i in range(1, hist.GetNbinsX() + 1)])
    

# Load raw events
traces = load_events(raw_filepath,series,nDumps_process)
nEvents = len(traces['PFS1']) # Number of events

height = []

for i in range(nEvents):
    print(f"Processing events {i}.....")
    tracelength = len(traces['PFS1'][0])
    pt = np.zeros(tracelength)
    ctr = 0
    for chan in chanNames:
        pt += traces[chan][i]
        ctr += 1
        #plt.plot(low_pass_filter(traces[chan], 0.01))
    pt /= ctr

    baseline = np.median(pt)
    OFTraces = af.ofAmp(pt,template,noisefft)
    OFTraces = np.roll(OFTraces,np.argmax(template)) #Align the traces so the peaks match up
    OFTraces += baseline

    minPeakHeight = baseline + 30
    minPeakWidth = 50

    peaks = af.getPeaks(OFTraces,height=minPeakHeight,width=minPeakWidth)
    xloc = peaks[1]

    #plt.figure(figsize=(20,6))
    #plt.plot(pt, label="raw trace")
    #plt.plot(low_pass_filter(pt, 0.01), label="low pass filter")
    #plt.plot(OFTraces, label="optimal filter")
    #plt.plot(xloc, OFTraces[xloc], ".", color='red', label="peaks")

    #plt.legend()

    height += list(peaks[2]-baseline)
#print ("OF height: ", height)
np.savetxt(outputfile,height)
#plt.hist(height, bins=70, range=(30,100), edgecolor='black')

# Add labels and title
#plt.xlabel('Height (ADC unit)')
#plt.ylabel('Count')
#plt.title('100V Z2 Si 23231120_193732 pulse height distribution in BORR events')

# Show the plot
#plt.show()