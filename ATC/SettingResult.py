#For some reason the pkl files in JupyterTimeStamps remember that the 'Setting' class came from a file called 'SettingResult', so this file needs to be here :(   Annoying legacy thing...

from typing import List

#This is the structure of the settings stored in the JupyterTimeStamps
class Setting():
    """Setting represents one setting in the Leakage Rate Study automation""" #TODO - give a better docstring, should include units of attributes and description if applicable
    
    def __init__(self, voltage:int=0, duration:int=0, bake:bool=False, bakeChannel:int=0, bakeDuration:int=0, comment:str='', skipDataCollection:bool=False, onBreakdownStepperChange:int=0, onBreakdownSelfVoltChange:int=0, onBreakdownShutOff:bool=True): #Volts, minutes, bool, int, str, bool, DeltaSettings(pos forw, neg back), Volts, bool #skipDataCollection before June 23rd skips storing data at end of setting, still adds to plot during run, after June 23rd it takes same data, just skips showing it in results summary
        #onBreakdownStepperChange moves the stepper by the amount given as well as by +1 for the next cycle. Ex. If you make it -1, next cycle will be the same setting
        self.voltage = voltage
        self.duration = duration
        self.bake = bake
        self.bakeChannel = bakeChannel
        self.bakeDuration = bakeDuration
        self.comment = comment
        self.skipDataCollection = skipDataCollection
        self.onBreakdownStepperChange = onBreakdownStepperChange
        self.onBreakdownSelfVoltChange = onBreakdownSelfVoltChange
        self.onBreakdownShutOff = onBreakdownShutOff
    
    def __repr__(self): 
        return "Setting voltage:%s duration:%s bake:%s bakeChannel:%s bakeDuration:%s comment:%s skipDataCollection:%s onBreakdownStepperChange:%s onBreakdownSelfVoltChange:%s onBreakdownShutOff:%s" % (self.voltage, self.duration, self.bake, self.bakeChannel, self.bakeDuration, self.comment, self.skipDataCollection, self.onBreakdownStepperChange, self.onBreakdownSelfVoltChange, self.onBreakdownShutOff)